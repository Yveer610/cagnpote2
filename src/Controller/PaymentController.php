<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Entity\Participant;
use App\Form\PaymentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/payment")
 */
class PaymentController extends AbstractController
{

        /**
     * @Route("/charge", name="payment_charge", methods="GET|POST")
     */
    public function charge(Request $request): Response
    {

            $amount = (int)$request->request->get("mount") * 100;

            try{
            \Stripe\Stripe::setApiKey('sk_test_4bAQhMmFT6PnYC8F78H6ptic');
            $charge = \Stripe\Charge::create(['amount' => $amount, 'currency' => 'eur', 'source' => $request->request->get("stripeToken")]);
            }
            catch (\Exception $e){
                $this->addFlash(
                    'error',
                    'Le paiement à échoué. Raison :' . $e->getMessage()
                );
                return $this->redirectRoute('campaign_pay', [
                "id" => $campaign_id
            ]);
            }

            $participant = new Participant();

            $participant->setCampaignId($request->request->get("campaign_id"));
            $participant->setName($request->request->get("name"));
            $participant->setEmail($request->request->get("email"));


            $em = $this->getDoctrine()->getManager();
            $em->persist ($participant);
            $em->flush();

            $payment = new Payment();

            $payment->setAmount($amount);

            $payment->setParticipantId($participant->getId());

            $em = $this->getDoctrine()->getManager();
            $em->persist ($payment);
            $em->flush();

            return $this->redirectToRoute('campaign_show',[
                "id" => $request->request->get("campaign_id")
            ]); 
    }
}