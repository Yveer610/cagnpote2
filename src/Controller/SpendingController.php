<?php

namespace App\Controller;

use App\Entity\Spending;
use App\Entity\Participant;
use App\Form\SpendingType;
use App\Entity\Campaign;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class SpendingController extends AbstractController
{

        /**
     * @Route("/spend", name="spending", methods="GET|POST")
     */
    public function spend(Request $request): Response
    {

            $amount = (int)$request->request->get("mount") * 100;
            

            $participant = new Participant();

            $participant->setCampaignId($request->request->get("campaign_id"));
            $participant->setName($request->request->get("name"));
            $participant->setEmail($request->request->get("email"));

            //dd($request);
            $em = $this->getDoctrine()->getManager();
            $em->persist ($participant);
            $em->flush();

            $spend = new Spending();
        
            $spend->setAmount($amount);
            $spend->setParticipant($participant);
            $spend->setLabel($label);

            $em = $this->getDoctrine()->getManager();
            $em->persist ($spend);
            $em->flush();

            return $this->redirectToRoute('campaign_show',[
                "id" => $request->request->get("campaign_id")
            ]); 
    }
}